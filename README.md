# azrs-tracking

# AZRS course project

Issue board covering common software development tools demonstrated on the project [network analyzer](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2023-2024/network-analyzer) as a part of the course Alati za razvoj softvera. 

Tools (to be) covered: 

### Versioning and control: 

- git "basic" usage
- git-hooks

### Debugging and profiling 

- GDB 
- valgrind
- gammaray

### Static code analysis

- clang-tidy

### Containerizaton 

- Docker 

### Automation pipelines
- CI

### Other 

- cmake 
- clang-format 
- bonus: terraform